package com.thinknyx.kafkademo2.controller;


import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class KafkaController {

    private KafkaTemplate<String, String> template;
    private String TOPIC_NAME = "demo";
    //private MyTopicConsumer consumer;
    public KafkaController(KafkaTemplate<String, String> template) {
        this.template = template;
        //this.myConsumer= myConsumer;
    }

    @GetMapping("/kafka/produce")
    public void produce(@RequestParam String message) {
        template.send(TOPIC_NAME, message);
    }

}
