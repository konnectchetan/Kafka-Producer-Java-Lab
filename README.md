## Instructions 
- First of all make sure `maven` is installed on the VM, just verify it with below command

    ```mvn --version```
- If the above stated command has given you error, then install maven otherwise move to the next step

    ```apt-get install maven```
- Switch to home directory
    
    ```cd /home/<user_name>```
- Get the code cloned
    
    ```git clone https://gitlab.com/konnectchetan/Kafka-Producer-Java-Lab```
- Change directory to code

    ```cd Kafka-Producer-Java-Lab```

- If you wish to change the topic name, then you need to edit the controller file (OPTIONAL)

    ```vi src/main/java/com/thinknyx/kafkademo2/controller/KafkaController.java```

- Run the app using 

    ```./mvnw spring-boot:run```
- Now access the application in browser
    - To produce message url will be

    ```your_vm_ip:8080/kafka/produce?message=SomeMessage```
- Verify the sent messages via consumer using cli, if topic name is different change it
    
    ```bin/kafka-console-consumer.sh --topic demo --bootstrap-server localhost:9092 --from-beginning```
